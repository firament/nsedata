using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace NseData
{
	public class Program
	{
		public static void Main(string[] args)
		{
			// CreateHostBuilder(args).Build().Run();
			Console.WriteLine($"Environment.Version: {System.Environment.Version}");
			Console.WriteLine("CWD = " + Environment.CurrentDirectory);
			Console.WriteLine("ENV = " + Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "PRODUCTION");

			// NLog: setup the logger first to catch any errors
			string lsNlogConfog =
					(
					Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT")
						== Microsoft.Extensions.Hosting.Environments.Development
					)
						? "NLog.Development.config"
						: "NLog.config"
						;
			// NLog: Initialize and Configure factory, manager
			NLog.Logger logger = NLog.Web.NLogBuilder
									.ConfigureNLog(lsNlogConfog)
									.GetCurrentClassLogger()
									;
			try
			{
				logger.Info("init main => public static void Main(string[] args)");
				logger.Info("Using log file {0}", lsNlogConfog);
				Console.WriteLine($"Using log config file {lsNlogConfog}");
				CreateHostBuilder(args).Build().Run();
			}
			catch (Exception ex)
			{
				logger.Error(ex, "App unexpected exception. Quitting.");
				throw;
			}
			finally
			{
				logger.Info("exit main => public static void Main(string[] args)");
				// NLog: Ensure to flush and stop internal timers/threads before application-exit 
				// (Avoid segmentation fault on Linux)
				NLog.LogManager.Shutdown();
			}

		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				})
				// NLog: Setup NLog for Dependency injection
				.ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
				})
				.UseNLog()
				;
	}
}
