﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace sak.trails.NseData.Helpers
{
	public class DataScraper
	{
		private static readonly HttpClient httpClient = new HttpClient { Timeout = new TimeSpan(0, 1, 0) };
		private static ILogger<DataScraper> log;
		private const bool TRY_CACHE = true;   // Turn off after debugging.

		public DataScraper(ILogger<DataScraper> logger)
		{
			log = logger;
			// Need to be set just once per instance
			ServicePointManager.Expect100Continue = true;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			//// CsvHelper.Configuration
			//CH_Config.HasHeaderRecord = true;
			//CH_Config.RegisterClassMap<CsvLineEntryMapv2>();
			//CH_Config.BadDataFound = (ReadingContext) => { Console.WriteLine(""); };
			//CH_Config.IgnoreBlankLines = true;
			//CH_Config.ReadingExceptionOccurred = (CsvHelperException ) => { Console.WriteLine(""); return false; };
		}

		public async Task<IEnumerable<sak.trails.NseData.Models.LineEntry>> GetDataEntries(DateTime BeginDate, DateTime EndDate)
		{
			// Assuming we will get validate data

			List<Models.LineEntry> lineEntries = new List<Models.LineEntry>();

			// Get data
			IEnumerable<Models.LineEntry> lstDayEntries;
			DateTime ldtCurr = BeginDate;
			try
			{
				do
				{
					Thread.Sleep(100);
					switch (ldtCurr.DayOfWeek)
					{
						case DayOfWeek.Sunday:
						case DayOfWeek.Saturday:
							Console.WriteLine("Skipping {0}", ldtCurr.ToString("ddd dd-MMM"));
							// continue;
							break;

						default:
							lstDayEntries = await GetDayEntries(ldtCurr);
							lineEntries.AddRange(lstDayEntries);
							break;
					}
					ldtCurr = ldtCurr.AddDays(1);
				} while (EndDate >= ldtCurr);
			}
			catch (Exception eX)
			{
				log.LogError(eX, "Error Processing File");
				Console.WriteLine(eX.Message);
			}

			return lineEntries;
		}

		private async Task<IEnumerable<Models.LineEntry>> GetDayEntries(DateTime ForDate)
		{
			log.LogInformation($"For Date {ForDate}");
			List<Models.LineEntry> lineEntries = new List<Models.LineEntry>();

			// Get Data
			string lsRawData;

			//
			// FOR LOCAL DEBUGGING, AVOID NEEDLESS CALLS
			lsRawData = TRY_CACHE ? Helpers.DataTestStub.GetDataForDate(ForDate) : string.Empty;
			//

			if (string.IsNullOrWhiteSpace(lsRawData))
			{
				lsRawData = await WebApiGettResponse(
								GetDayURL(ForDate)
								)
								;
			}
			if (string.IsNullOrWhiteSpace(lsRawData))
			{
				return lineEntries;
			}

			// Cache response for inspection and debug
			Helpers.DataTestStub.CacheResponse(ForDate, lsRawData);

			// Parse Data
			string lsLine;
			Exception lexLine;
			string[] lasLines = lsRawData.Split('\n');
			Models.LineEntry loEntry;
			for (int vI = 2; vI < lasLines.Length; vI++)
			{
				lsLine = lasLines[vI].Trim();
				if (string.IsNullOrWhiteSpace(lsLine) || lsLine.StartsWith("TOTAL,", StringComparison.OrdinalIgnoreCase))
				{
					continue;
				}
				loEntry = new Models.LineEntry();
				lexLine = loEntry.Populate(lasLines[vI], ForDate);
				lineEntries.Add(loEntry);

				// Log any processing error
				if (lexLine != null)
				{
					log.LogWarning(lexLine, "Line Processing Error for Date {0}", ForDate);
				}
			}

			return lineEntries;
		}

		private string GetDayURL(DateTime ForDate)
		{
			const string URL_FORMAT = "https://www1.nseindia.com/content/nsccl/fao_participant_oi_{0}.csv";
			string lsDayURL = string.Empty;
			try
			{
				lsDayURL = string.Format(URL_FORMAT, ForDate.ToString("ddMMyyyy"));
			}
			catch (Exception exf)
			{
				Console.WriteLine(exf.Message);
			}
			return lsDayURL;
		}

		private async Task<string> WebApiGettResponse(string ReqURL)
		{
			string lsReqURL = ReqURL;
			string lsResponse = string.Empty;
			try
			{
				lsResponse = await httpClient.GetStringAsync(lsReqURL);
			}
			catch (HttpRequestException hrX)
			{
				Thread.Sleep(50);
				log.LogError(hrX.Message, lsReqURL);
				Console.WriteLine(lsReqURL);
			}
			catch (Exception eX)
			{
				Thread.Sleep(50);
				log.LogError(eX.Message, lsReqURL);
			}
			return lsResponse;
		}
	}
}