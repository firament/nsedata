﻿using System;
using System.IO;

namespace sak.trails.NseData.Helpers
{
	public static class DataTestStub
	{
		private const string FILE_NAME_FORMAT = "fao_participant_oi_{0}.csv";
		private const string FILE_PATH = "t30_test_data";
		private const string SOURCE_DATE_FORMAT = "ddMMyyyy";   // 28052020

		public static bool CacheResponse(DateTime dDate, string DataContent)
		{
			bool lbResult = false;
			try
			{
				Console.WriteLine("Debug Here");
				string lsDir = Path.Combine(
										Environment.CurrentDirectory
										, FILE_PATH
										);
				if (!Directory.Exists(lsDir)) Directory.CreateDirectory(lsDir);

				string lsFilePath = Path.Combine(
										lsDir
										, string.Format(
											string.Format(FILE_NAME_FORMAT, dDate.ToString(SOURCE_DATE_FORMAT))
											)
										);
				if (!File.Exists(lsFilePath)) File.WriteAllText(lsFilePath, DataContent);
				lbResult = true;
			}
			catch (Exception eX)
			{
				Console.WriteLine(eX.Message);
			}
			return lbResult;
		}

		public static string GetDataForDate(DateTime dDate)
		{
			string lsData = string.Empty;
			string lsFilePath = Path.Combine(
									Environment.CurrentDirectory
									, FILE_PATH
									, string.Format(
										string.Format(FILE_NAME_FORMAT, dDate.ToString(SOURCE_DATE_FORMAT))
										)
									);
			if (!File.Exists(lsFilePath)) return lsData;
			try
			{
				using StreamReader loReader = new StreamReader(lsFilePath);
				lsData = loReader.ReadToEnd();
			}
			catch (Exception eX)
			{
				Console.WriteLine(eX.Message);
			}
			return lsData;
		}
	}
}