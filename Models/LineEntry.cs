﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using CM = sak.trails.NseData.Models.Common;

namespace sak.trails.NseData.Models
{

	public static class Common
	{
		public static string[] ColHeaderText =
		{
			"Entry Date",
			"Client Type",
			"Future Index Long",
			"Future Index Short",
			"Future Stock Long",
			"Future Stock Short",
			"Option Index Call Long",
			"Option Index Put Long",
			"Option Index Call Short",
			"Option Index Put Short",
			"Option Stock Call Long",
			"Option Stock Put Long",
			"Option Stock Call Short",
			"Option Stock Put Short",
			"Total Long Contracts",
			"Total Short Contracts",
		};

		public enum ColumnHeaders
		{
			Client_Type,
			Future_Index_Long,
			Future_Index_Short,
			Future_Stock_Long,
			Future_Stock_Short,
			Option_Index_Call_Long,
			Option_Index_Put_Long,
			Option_Index_Call_Short,
			Option_Index_Put_Short,
			Option_Stock_Call_Long,
			Option_Stock_Put_Long,
			Option_Stock_Call_Short,
			Option_Stock_Put_Short,
			Total_Long_Contracts,
			Total_Short_Contracts,
			Entry_Date,
		}

	}

	public class LineEntry
	{
		#region Object Properties

		public string Client_Type { get; set; }
		public DateTime EDate { get; set; }
		public int Future_Index_Long { get; set; }
		public int Future_Index_Short { get; set; }
		public int Future_Stock_Long { get; set; }
		public int Future_Stock_Short { get; set; }
		public int Option_Index_Call_Long { get; set; }
		public int Option_Index_Call_Short { get; set; }
		public int Option_Index_Put_Long { get; set; }
		public int Option_Index_Put_Short { get; set; }
		public int Option_Stock_Call_Long { get; set; }
		public int Option_Stock_Call_Short { get; set; }
		public int Option_Stock_Put_Long { get; set; }
		public int Option_Stock_Put_Short { get; set; }
		public int Total_Long_Contracts { get; set; }
		public int Total_Short_Contracts { get; set; }

		#endregion Object Properties

		private readonly ILogger<LineEntry> log;

		public LineEntry()
		{
		}

		[Obsolete("Do not use till injection is fixed", true)]
		public LineEntry(ILogger<LineEntry> logger)
		{
			log = logger;
		}

		public Exception Populate(string CsvLine, DateTime EntryDate)
		{
			Exception exPop = null;
			if (string.IsNullOrWhiteSpace(CsvLine)) return exPop;

			// Split, check col count
			string[] lasEntry = CsvLine.Split(',');

			// Boundary case 1:	If any quotes, scrub and attempt process
			if (CsvLine.Contains('"') && CsvLine.Contains('.')) { (exPop, lasEntry) = Boundarycase_1(CsvLine); }

			if (lasEntry.Length < 15) return new Exception(
											  string.Format("Entry count mismatch. Inspect the data. Data = {0}, Date = {1}", CsvLine, EntryDate)
											, exPop
											);
			lasEntry.Append(EntryDate.ToString("dd MMM yyyy"));

			try
			{
				this.EDate = EntryDate;
				this.Client_Type = lasEntry[(int)CM.ColumnHeaders.Client_Type];
				this.Future_Index_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Future_Index_Long]);
				this.Future_Index_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Future_Index_Short]);
				this.Future_Stock_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Future_Stock_Long]);
				this.Future_Stock_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Future_Stock_Short]);
				this.Option_Index_Call_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Index_Call_Long]);
				this.Option_Index_Put_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Index_Put_Long]);
				this.Option_Index_Call_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Index_Call_Short]);
				this.Option_Index_Put_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Index_Put_Short]);
				this.Option_Stock_Call_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Stock_Call_Long]);
				this.Option_Stock_Put_Long = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Stock_Put_Long]);
				this.Option_Stock_Call_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Stock_Call_Short]);
				this.Option_Stock_Put_Short = int.Parse(lasEntry[(int)CM.ColumnHeaders.Option_Stock_Put_Short]);
				this.Total_Long_Contracts = int.Parse(lasEntry[(int)CM.ColumnHeaders.Total_Long_Contracts]);
				this.Total_Short_Contracts = int.Parse(lasEntry[(int)CM.ColumnHeaders.Total_Short_Contracts]);
			}
			catch (Exception eX)
			{
				exPop = new Exception(string.Format("Error parsing line '{0}' for Date {1}", CsvLine, EntryDate)
					, eX
					);
			}
			return exPop;
		}

		private static (Exception, string[]) Boundarycase_1(string lsRaw)
		{
			Exception leX = null;
			string[] lasEntry = Array.Empty<string>();
			List<string> lstElems = new List<string>();

			string vsElem;
			try
			{
				// Scrub
				lasEntry = lsRaw.Replace("\",\"", "|").Replace(",\"", "|").Replace("\",", "|").Split('|');
				// remove all formatting from numbers
				for (int vI = 0; vI < lasEntry.Length; vI++)
				{
					vsElem = lasEntry[vI];
					if (vsElem.Contains('.')) { lstElems.Add(vsElem.Replace(",", "").Split('.')[0]); }
					else if (vsElem.Contains(",")) { foreach (string vsE in vsElem.Split(",")) { lstElems.Add(vsE.Trim()); } }
					else { lstElems.Add(vsElem.Trim()); }
				}
				lasEntry = lstElems.ToArray();
			}
			catch (Exception eX)
			{
				leX = new Exception($"Error processing line '{lsRaw}'", eX);
			}
			return (leX, lasEntry);
		}

		/*
		[Obsolete("Use only for debugging.")]
		private string ArrayAsString(string[] psEntries)
		{
			string lsResult = string.Empty;
			for (int vI = 0; vI < psEntries.Length; vI++)
			{
				lsResult += psEntries[vI] + ",";
			}
			return lsResult.TrimEnd(',');
		}
		*/
	}

}