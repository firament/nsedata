﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NseData.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace sak.trails.NseData.Controllers
{
	public class HomeController : Controller
	{
		private const string BUILD_TAG = "63-7276-5279-6529-4720";
		private readonly IHostApplicationLifetime AppLifetime;
		private readonly Helpers.DataScraper DS;
		private readonly ILogger<HomeController> log;

		public HomeController(ILogger<HomeController> logger, IHostApplicationLifetime appLifetime, Helpers.DataScraper _ds)
		{
			// Console.WriteLine(DateTime.UtcNow.Ticks.ToString("00-0000-0000-0000-0000"));
			log = logger;
			AppLifetime = appLifetime;
			DS = _ds;
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		public ActionResult<string> Reboot()
		{
			log.LogInformation("Shutting down the application now.");
			AppLifetime.StopApplication();
			return new ActionResult<string>("Application shutting down");
		}

		public async Task<IActionResult> SumData(DateTime BeginDate, DateTime EndDate)
		{
			log.LogInformation("SumData Begin");
			const int MAX_DAY_RANGE = 5;
			ViewData["BuildTag"] = BUILD_TAG;

			if (BeginDate == DateTime.MinValue && EndDate == DateTime.MinValue)
			{
				// Form is not yet filled, show empty list
				ViewData["OutMesg"] = "Enter valid dates to get data";
				return View(new List<Models.LineEntry>());
			}

			string lsMesg;
			// Set all times to midnight
			DateTime ldtBegin = DateTime.Parse(BeginDate.ToString("yyyy-MMM-dd 00:00:00"));
			DateTime ldtFinish = DateTime.Parse(EndDate.ToString("yyyy-MMM-dd 00:00:00"));
			// DateTime ldtCurr = ldtBegin;

			// Validate boundaries
			/*
			 * begin should not be null
			 * if end is null, set same as begin
			 * begin should be <= end
			 * end should be less than current
			 * diff should not be > MAX_DAY_RANGE
			 */
			if (ldtFinish == DateTime.MinValue)
			{
				ldtFinish = ldtBegin;
			}
			int liRange = (int)ldtFinish.Subtract(ldtBegin).TotalDays;
			if (liRange < 0)
			{
				lsMesg = "Begin Date should be earlier than or same as Finish date.";
			}
			else if (liRange > MAX_DAY_RANGE)
			{
				lsMesg = $"Cannot process for more than {MAX_DAY_RANGE} days.";
			}
			else if (ldtFinish > DateTime.Today.AddDays(-1.0))
			{
				lsMesg = "Date range can only be for historical data.";
			}
			else if (ldtBegin < DateTime.Today.AddDays(-60d))
			{
				lsMesg = "Start date cannot be older than 60 days";
			}
			else
			{
				lsMesg = $"Processed data for {liRange + 1} days";
			}
			// Show validation and error message, but do not block.

			ViewData["OutMesg"] = lsMesg;
			ViewData["BeginDate"] = ldtBegin;
			ViewData["FinisDate"] = ldtFinish;
			log.LogInformation("{{From: {0}, To: {1}, Mesg: {2}", ldtBegin, ldtFinish, lsMesg);

			IEnumerable<Models.LineEntry> llstEntries = await DS.GetDataEntries(ldtBegin, ldtFinish);
			return View(llstEntries);
		}
	}
}